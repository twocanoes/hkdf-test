//
//  main.m
//  hkdf-test
//
//  Created by Timothy Perfitt on 9/26/20.
//

/*expected results
 QRCode secret:
 d7ef75e6194fe414f9363150eb10e152

 eidKey:
 6f2769b9d9f2c37dc78c76f4f42faf6aff2b0bd310763cc164e5c83088740566
*/
#import <Foundation/Foundation.h>
#include "openssl/hkdf.h"
#include "openssl/sha.h"
#include "openssl/digest.h"
#import "NSData+HexString.h"
void Derive(uint8_t* res_out,
            size_t out_len,
            uint8_t* secret,size_t secret_size,
             uint8_t* nonce, size_t nonce_size,
            uint8_t*info,size_t info_size)


{
  HKDF(res_out, out_len, EVP_sha256(),
       secret,secret_size,
       nonce, nonce_size,
       info,info_size);


    
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        uint8_t res[32];

        uint8_t qr_code[]={ 0xd7, 0xef, 0x75, 0xe6, 0x19, 0x4f, 0xe4, 0x14, 0xf9, 0x36, 0x31, 0x50,
            0xeb, 0x10, 0xe1, 0x52};

        uint8_t nonce[]={ };

        uint32 info=1;

        Derive(res, sizeof(res), qr_code, sizeof(qr_code), nonce, sizeof(nonce), (uint8_t*)(&info), sizeof(info));

        uint8 expected_data[]={0x6f, 0x27, 0x69, 0xb9, 0xd9, 0xf2, 0xc3, 0x7d, 0xc7, 0x8c, 0x76, 0xf4,
            0xf4, 0x2f, 0xaf, 0x6a, 0xff, 0x2b, 0x0b, 0xd3, 0x10, 0x76, 0x3c, 0xc1,
            0x64, 0xe5, 0xc8, 0x30, 0x88, 0x74, 0x05, 0x66};

        NSData *expectedData=[NSData dataWithBytes:expected_data length:sizeof(expected_data)];
        NSData *resultData=[NSData dataWithBytes:res length:sizeof(res)];

        NSLog(@"result: %@",resultData.hexString);

        if ([expectedData isEqualToData:resultData]){
            NSLog(@"match!");
        }
        else {
            NSLog(@"no match!");
        }
    }
    return 0;
}
